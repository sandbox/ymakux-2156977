<?php

/**
 * Settings form builder
 */
function unisender_settings_form($form, $form_state) {
  $form['unisender_api_key'] = array(
    '#title' => t('Api key'),
    '#type' => 'textfield',
    '#default_value' => variable_get('unisender_api_key', ''),
    '#required' => TRUE,
    '#description' => t('A string with your personal API key which you can get in your account at unisender.com.'),
  );
  return system_settings_form($form);
}

/**
 * Newsletters overview form
 */
function unisender_newsletters_form($form, $form_state) {
  $rows = array();
  foreach(unisender_newsletters() as $newsletter) {
    $links = array();
    $links[] = array(
      'href'  => 'admin/config/services/unisender/newsletter/' . $newsletter['id'] . '/subscribe',
      'title' => t('Subscribe'),
      'query' => array('destination' => current_path()),
    );
    $rows[$newsletter['id']] = array(
      check_plain($newsletter['id']),
      check_plain($newsletter['title']),
      theme('links', array('links' => $links, 'attributes' => array('class' => array('links', 'inline')))),
    );
  }
  
  $header = array(t('ID'), t('Name'), t('Operations'));
  $form['newsletters']['#markup'] = theme('table', array('rows' => $rows, 'header' => $header, 'empty' => t('You have no newsletters')));
  
  $form['refresh'] = array(
    '#type' => 'submit',
    '#value' => t('Refresh'),
    '#submit' => array('unisender_newsletters_form_refresh'),
  );
  return $form;
}

/**
 * Submit callback for "Refrech" button.
 */
function unisender_newsletters_form_refresh($form, $form_state) {
  // Empty cached newsletters data so we can get frech data from unisender.com
  cache_clear_all('unisender_newsletters', 'cache');
}

/**
 * Subscribe form builder
 */
function unisender_subscribe_form($form, &$form_state, $newsletter) {

  $form['#tree'] = TRUE;
  $form_state['newsletter'] = $newsletter;
  $settings = variable_get('unisender_newsletter_settings', array());
  
  $show_email = TRUE;
  if(isset($settings[$newsletter['id']]['fields']['email']) && !$settings[$newsletter['id']]['fields']['email']) {
    $show_email = FALSE;
  }
  
  $show_phone = FALSE;
  if(isset($settings[$newsletter['id']]['fields']['phone']) && $settings[$newsletter['id']]['fields']['phone']) {
    $show_phone = TRUE;
  }

  if($show_email) {
    $form['fields']['email'] = array(
      '#title' => t('E-mail'),
      '#type' => 'textfield',
      '#required' => TRUE,
      '#size' => 32,
      '#element_validate' => array('unisender_validate_email'),
    );
  }
  
  if($show_phone) {
    $form['fields']['phone'] = array(
      '#title' => t('Phone'),
      '#type' => 'textfield',
      '#required' => TRUE,
      '#size' => 32,
      '#required' => TRUE,
      '#element_validate' => array('unisender_validate_phone'),
      '#description' => t('Mobile phone number, for example +380671111111'),
    );
  }
  
  $form['subscribe'] = array(
    '#type' => 'submit',
    '#value' => t('Subscribe'),
    '#submit' => array('unisender_subscribe_form_submit'),
    '#limit_validation_errors' => array(array('fields')),
  );
  return $form;
}

/**
 * Submit callback for subscribe form
 */
function unisender_subscribe_form_submit($form, &$form_state) {
  form_state_values_clean($form_state);
  $newsletter_id = $form_state['newsletter']['id'];
  $params = $form_state['values'] + array('list_ids' => $newsletter_id, 'tags' => 'site');
  $results = unisender_query('subscribe', $params);
  
  if(!empty($results['error'])) {
    form_set_error(t('An error occured:@error', array('@error' => $results['code'])), 'error');
    return;
  }
  drupal_set_message(t('You have been subscribed.'));
}
